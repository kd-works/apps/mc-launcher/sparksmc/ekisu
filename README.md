# Ekisu

Java executable who can extract .pack.xz, .pack & .xz files

## Usage

### .pack.xz files
```sh
java -jar Ekisu.jar -packxz filepath1.pack.xz,filepath2.pack.xz,...
```

### .pack files
```sh
java -jar Ekisu.jar -pack filepath1.pack,filepath2.pack,...
```

### .xz files
```sh
java -jar Ekisu.jar -xz filepath1.xz,filepath2.xz,...
```
